<?php

require_once 'vendor/autoload.php';

use Firebase\JWT\JWT;

// Tu clave secreta (debe mantenerse en secreto)
$secretKey = 'tu_clave_secreta';

// Datos a incluir en el token (payload)
$payload = [
    'user_id' => 123,
    'username' => 'john_doe',
    'exp' => time() + 3600, // Tiempo de expiración del token (1 hora desde ahora)
];

try {
    // Codificar (encode) el payload en un JWT
    $token = JWT::encode($payload, $secretKey, 'HS256');
    
    echo "JWT Token: $token\n";

    // Decodificar (decode) el token
    $decoded = JWT::decode($token, $secretKey, ['HS256']);

    echo "Decoded Token:\n";
    print_r($decoded);
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
}
